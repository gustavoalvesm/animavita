import React, { useEffect, useState } from 'react';
import cn from 'classnames';
import axios from 'axios';
import './index.css';
import Card from '../Card';

function CardList() {

    const [characters, setCharacters] = useState([]);
    const [error, setError] = useState(null);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        axios.get('https://kitsu.io/api/edge/characters')
            .then(res => {
                const characters = res.data.data;
                setCharacters(characters);
                setLoading(false);
            }).catch(err => {
                setError("Tivemos um pequeno problema interno... Por favor, tente mais tarde!");
                setLoading(false);
            })
    }, []);

    return (
        <>
            {error ?
                <div className={cn('alert', 'alert-danger')}>{error}</div>
                :
                loading ?
                    <div className={cn("spinner-border text-primary")}>
                        <span className={cn("sr-only")}>Carregando...</span>
                    </div>
                    :
                    characters.length > 0 ?
                        <div className={cn('row', 'row-cols-1', 'row-cols-sm-2', 'row-cols-md-3', 'row-cols-lg-4')}>
                            {characters.map(character =>
                                <Card character={character} />
                            )}
                        </div>
                        :
                        <div>Nenhum personagem encontrado.</div>
            }
        </>
    );
}

export default CardList;