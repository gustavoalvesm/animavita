import React from 'react';
import cn from 'classnames';

function Footer() {
    return (
        <footer className={cn('container', 'text-center', 'py-3')}>
            <p className={cn('text-secondary')}><b>Telavita 2019</b> ® Todos os direitos reservados.</p>
        </footer>
    );
}

export default Footer;