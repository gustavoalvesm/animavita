import React, { useEffect, useState } from 'react';
import cn from 'classnames';
import axios from 'axios';
import {
    useParams
} from "react-router-dom";

function CharacterDetail() {

    const [character, setCharacter] = useState([]);
    const [loading, setLoading] = useState(true);
    let { id } = useParams();

    useEffect(() => {
        axios.get(`https://kitsu.io/api/edge/characters/${id}`)
            .then(res => {
                const character = res.data.data;
                setCharacter(character);
                setLoading(false);
            }).catch(err => {
                window.location = "/";
                setLoading(false);
            })
    }, []);

    return (
        <>
            {
                loading ?
                    <div className={cn("spinner-border", "text-primary")}>
                        <span className={cn("sr-only")}>Carregando...</span>
                    </div>
                    :
                    <div className={cn("row", "pt-3")}>
                        <div className={cn("col-md-3", "pb-5")}>
                            <h2>{character.attributes.name}</h2>
                            <img src={character.attributes.image.original} class="rounded" alt={character.attributes.name} />
                        </div>
                        <div className={cn("col-md-9", "text-justify")}>
                            <div dangerouslySetInnerHTML={{ __html: character.attributes.description }} />
                        </div>
                    </div>
            }
        </>
    );
}

export default CharacterDetail;