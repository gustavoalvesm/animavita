import React from 'react';
import cn from 'classnames';

function Card(props) {

    const { character } = props;

    function getShortDescription(fullDescription) {
        let shortDescription = fullDescription.replace(/(<([^>]+)>)/ig, "");
        shortDescription = shortDescription.split(".");
        shortDescription = shortDescription.length > 0 ? shortDescription[0] : shortDescription;
        return `${shortDescription}.`;
    }

    return (
        <article className={cn('col', 'mb-4')}>
            <div className={cn('card', 'shadow-sm')}>
                <img
                    src={character.attributes.image.original}
                    className={cn('card-img-top', 'card-img-adjust')}
                    alt={character.attributes.name}
                />
                <div className={cn('card-body')}>
                    <h5 className={cn('card-title')}>{character.attributes.name}</h5>
                    <p className={cn('card-text', 'text-secondary', 'text-center', 'card-character-description')}>
                        <small>
                            {
                                character.attributes.otherNames &&
                                character.attributes.otherNames.length > 0 &&
                                character.attributes.otherNames.join(' • ')}
                        </small>
                    </p>
                    <p className={cn('card-text', 'text-secondary', 'text-justify', 'card-character-description')}>
                        {getShortDescription(character.attributes.description)}
                    </p>
                    <a href={`/character/${character.id}`} className={cn('font-weight-bold')}>Ver perfil completo</a>
                </div>
            </div>
        </article>
    );
}

export default Card;