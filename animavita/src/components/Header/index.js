import React from 'react';
import cn from 'classnames';

function Header() {
    return (
        <header className={cn('container', 'text-center', 'pt-5')}>
            <a href="/">
                <h1 className={cn('text-primary')}>AnimaVita</h1>
            </a>
            <h3 className={cn('text-secondary', 'font-weight-normal')}>Consultas online com animes e mangás</h3>
        </header>
    );
}

export default Header;