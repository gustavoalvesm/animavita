import React from 'react';
import cn from 'classnames';

function Content(props) {
    return (
        <main className={cn('container', 'text-center', 'py-5')}>
            {props.children}
        </main>
    );
}

export default Content;