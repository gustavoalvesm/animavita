import React from 'react';
import Header from './components/Header';
import Content from './components/Content';
import Footer from './components/Footer';
import CardList from './components/CardList';
import CharacterDetail from './components/CharacterDetail';
import {
	BrowserRouter as Router,
	Switch,
	Route,
	Redirect
} from "react-router-dom";

function App() {
	return (
		<>
			<Header />
			<Router>
				<Switch>
					<Route path="/character/:id">
						<Content>
							<CharacterDetail />
						</Content>
					</Route>
					<Route path="/character">
						<Redirect to="/" />
					</Route>
					<Route path="/">
						<Content>
							<CardList />
						</Content>
					</Route>
				</Switch>
			</Router>

			<Footer />
		</>
	);
}

export default App;
